import smtplib, ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
#sendmail function gets recever and sender in form of parameter
def sendmail(sender_email,receiver_email):
    msg = "This mail is sent from " + sender_email + " to " + receiver_email
    message = MIMEMultipart()
    message["Subject"] = "Text mail assignment"
    message["From"] = sender_email
    message["To"] = receiver_email
    message.attach(MIMEText(msg, 'plain'))
    server = smtplib.SMTP("smtp1.hpe.com")
    server.sendmail(sender_email, receiver_email, message.as_string())

