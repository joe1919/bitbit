from utils import sendmail
import os, fnmatch
pattern="Email.txt"
path="C:"
listoffiles = []

#traveling through all the files in path folder
for root, dirs, files in os.walk(path):
    for name in files:
        #looking for name of file passed as parameter
        if fnmatch.fnmatch(name, pattern):
            listoffiles.append(os.path.join(root, name))
fileName=open(listoffiles[0])
lines=[]
#converting text file into list
for i in fileName.readlines():
    lines.append(i)
lines.pop(0)
#splitiing mail ids into sender and receiver by using strip and split function
for x in range(0, len(lines)):
    my_string = lines[x]
    result = [x.strip() for x in my_string.split(',')]
    sender_email = result[0]
    receiver_email = result[1]
    #calling send mail function in util file
    sendmail(sender_email,receiver_email)
